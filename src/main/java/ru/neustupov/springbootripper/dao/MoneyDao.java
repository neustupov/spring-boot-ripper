package ru.neustupov.springbootripper.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.neustupov.springbootripper.model.Bank;

@Repository
public interface MoneyDao extends CrudRepository<Bank, String> {

}
