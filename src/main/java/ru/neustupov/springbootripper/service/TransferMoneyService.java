package ru.neustupov.springbootripper.service;

public interface TransferMoneyService {

  int transfer(String name, Integer amount);
}
