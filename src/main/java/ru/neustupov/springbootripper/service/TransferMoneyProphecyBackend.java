package ru.neustupov.springbootripper.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.neustupov.springbootripper.dao.MoneyDao;
import ru.neustupov.springbootripper.model.Bank;

@Service
@AllArgsConstructor
public class TransferMoneyProphecyBackend implements TransferMoneyService {

  private final ProphetService prophetService;

  private final MoneyDao dao;

  @Override
  public int transfer(String name, Integer amount) {
    int result = -1;
    if (prophetService.willSurvive(name)) {
      int money = dao.findById("IronBank").get().getMoney();
      if (money > amount) {
        result = amount;
        money -= amount;
        dao.save(new Bank("IronBank", money));
      }
    }
    return result;
  }
}
