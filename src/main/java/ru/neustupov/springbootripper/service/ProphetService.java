package ru.neustupov.springbootripper.service;


public interface ProphetService {

  boolean willSurvive(String name);
}
