package ru.neustupov.springbootripper.controller;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.neustupov.springbootripper.dao.MoneyDao;
import ru.neustupov.springbootripper.model.Bank;
import ru.neustupov.springbootripper.service.TransferMoneyService;

@Log4j2
@RestController
@RequiredArgsConstructor
public class IronBankController {

  private final TransferMoneyService moneyService;

  private final MoneyDao dao;

  private Logger logger = LoggerFactory.getLogger(IronBankController.class);

  @GetMapping("/credit")
  public String credit(@RequestParam String name, @RequestParam Integer amount) {

    logger.info("/credit path");

    int resultDeposit = moneyService.transfer(name, amount);

    if (resultDeposit == -1) {
      return "Rejected " + name;
    }

    return "Credit approved for " + name + " money: " + amount;
  }

  @GetMapping("/state")
  public Integer currentState() {
    log.info("/state path");
    List<Bank> list = new ArrayList<>();
    dao.findAll().forEach(list::add);
    return list.get(0).getMoney();
  }
}
