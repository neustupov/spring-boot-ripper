package ru.neustupov.springbootripper;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import ru.neustupov.springbootripper.dao.MoneyDao;
import ru.neustupov.springbootripper.model.Bank;

@SpringBootApplication
public class SpringBootRipperApplication {

  public static void main(String[] args) {
    SpringApplicationBuilder builder = new SpringApplicationBuilder(
        SpringBootRipperApplication.class);
    builder.headless(false);
    ConfigurableApplicationContext context = builder.run(args);
    MoneyDao moneyDao = context.getBean(MoneyDao.class);
    moneyDao.save(new Bank("IronBank", 1000));

  }

}

