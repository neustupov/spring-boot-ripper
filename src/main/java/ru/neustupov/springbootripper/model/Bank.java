package ru.neustupov.springbootripper.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Bank {

  @Id
  @Getter
  private String name;

  @Getter
  private Integer money;

}
