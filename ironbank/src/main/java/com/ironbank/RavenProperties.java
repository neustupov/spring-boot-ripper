package com.ironbank;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("raven")
public class RavenProperties {

  List<String> where;
}
