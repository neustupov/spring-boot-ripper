package com.ironbank.annotation;

import javax.swing.JOptionPane;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class OnProductionCondition implements Condition {

  @Override
  public boolean matches(ConditionContext conditionContext,
      AnnotatedTypeMetadata annotatedTypeMetadata) {
    return JOptionPane.showConfirmDialog(null, "Production?") == 0;
  }
}
